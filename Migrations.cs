﻿using Orchard.Data.Migration;
using Orchard.ContentManagement.MetaData;
using Orchard.Core.Contents.Extensions;

namespace Datwendo.Search
{
    public class Migrations : DataMigrationImpl {
        public int Create() {

            // Create a Menu Item
            ContentDefinitionManager.AlterTypeDefinition("SearchMenuItem", type => type
                .WithPart("SearchFormPart")    
                .WithPart("MenuPart")           // Required so that the Navigation system can attach our custom menu items to a menu
                .WithPart("CommonPart")         
                .WithPart("IdentityPart")       
                .DisplayedAs("Search Menu Item")     

                // The value of the Description setting will be shown in the Navigation section where our custom menu item type will appear
                .WithSetting("Description", "Include a search box inside your Menu.")

                // Required by the Navigation module
                .WithSetting("Stereotype", "MenuItem")

                // We don't want our menu items to be draftable
                .Draftable(false)

                // We don't want the user to be able to create new items outside of the context of a menu
                .Creatable(false)
                );



            return 1;
        }
    }
}