﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Orchard.Mvc.Routes;

namespace Datwendo.SearchMenu {
    public class Routes : IRouteProvider {

        public void GetRoutes(ICollection<RouteDescriptor> routes) {
            foreach (var routeDescriptor in GetRoutes())
                routes.Add(routeDescriptor);
        }

        public IEnumerable<RouteDescriptor> GetRoutes() {
            return new[] {
                new RouteDescriptor {
                    Priority = 10,
                    Route = new Route("Recherche/{searchIndex}",
                        new RouteValueDictionary {
                            {"area", "Datwendo.SearchMenu"},
                            {"controller", "Search"},
                            {"action", "Index"},
                            {"searchIndex", UrlParameter.Optional}
                        },
                        null,
                        new RouteValueDictionary {
                            {"area", "Datwendo.SearchMenu"}
                        },
                        new MvcRouteHandler())
                }
            };
        }
    }
}
