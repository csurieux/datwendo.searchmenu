# Datwendo.SearchMenu is an Orchard module providing a Search MenuItem based on Orchard.Search. #

* Orchard 1.9.x module providing a Search MenuItem based on Orchard.Search
* version 1.0
* To install: Clone the repo in Modules folder of Orchard solution (name the folder Datwendo.SearchMenu if no done by default), add it to solution
* Christian Surieux - Datwendo - 2016
* License Apache v2